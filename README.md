# Replica de clúster de BD de VIR a ORE

## 1. Descripción  📢

Este proyecto tiene por objetivo replicar clústeres de base datos mediante la creación de snapshots en las regiones de Virginia y Oregón y luego eliminar los generados después de 4 días. A partir del ultimo snapshot se creara una BD en la región de Oregón.

## 2. Requerimiento 📋

Fue solicitado el día 04/02/2021 con ticket N°1807286 . Se requería sacar un snapshot cada 12 horas de dos clústeres de base de datos que están en la región de Virginia.

* dbcluster-p01-vir
* dbcluster-p02.vir

Copiar los snapshot  en Oregon y mantener una BD encendida con la ultima copia. Eliminar los snapshot de VIRGINA y OREGON generados después de 4 días. 

**NOTA:** Esta BD en OREGON  tendrá que ser una db.t3.medium

## 3. Configuración 🔧

En el AWX de Visanet http://18.209.210.201 están definidos los Job Templates y Workflows.

- copy-db-cluster-snapshots
- create-db-cluster-snapshot
- remove-old-db-cluster-snapshots
- restore-db-cluster-snapshots
- workflow-db-cluster-snapshot

Para ejecutar estos playbooks necesitas crear una credencial de AWS en el servicio IAM y agregar tu credencial en AWX.

**IMPORTANTE:** Si deseas crear el mismo reporte para otras regiones o cuentas AWS del cliente, solo falta duplicar dichos job templates y workflows, para luego modificar sus variables y credenciales AWS.

## 4. Lógica de funcionamiento ⚙️ 

### 4.1. Workflow <u>*workflow-db-cluster-snapshot*</u>

#### 4.1.1. Job template <u>*create-db-cluster-snapshot*</u>

1. Recibe como variables de entrada region y databases (lista). Si estas variables no son definidas entonces no seguirá el flujo de ejecución y pedirá que ingrese las variables mencionadas.
2. Se creara un snapshot del clúster de base de datos que has definido como primero elemento. Le pondrá de nombre al snapshot : nombre de la base de datos (primer elemento) + fecha y hora actual.
3. Se creara un snapshot del clúster de base de datos que has definido como segundo elemento. Le pondrá de nombre al snapshot : nombre de la base de datos (segundo elemento)+ fecha y hora actual.
4. Se mostrara el resultado de la creación ,indicando detalles en formato JSON.

#### 4.1.2. Job template <u>*copy-db-cluster-snapshots*</u>

1. Recibe como variables de entrada regions (lista) y databases (lista). Si estas variables no son definidas entonces no seguirá el flujo de ejecución y pedirá que ingrese las variables mencionadas.
2. Se pausa el flujo de ejecución por 3 minutos ,esto es porque el job template que crea los snapshot deja a ambas bases de datos en un estado no disponible para realizar la copia.
3. Continuando con el flujo de ejecución lo siguiente que se realiza es guardar en dos variables (last_snapshot_arn1 y last_snapshot_arn2) con el ARN del ultimo elemento creado en la primera y segunda base de datos.
4. Se guardan dos variables (snapshot_identifier1 y snapshot_identifier2) con el nombre del ultimo snapshot de la primera y segunda base de datos.
5. En la dos ultimas tareas se llaman a las variables antes mencionadas para realizar la copia de estos a otra región. Uso la región 2 porque  es donde copiaran los snapshots.


### 4.2. Job template <u>*remove-old-db-cluster-snapshots*</u>

1. Recibe como variables de entrada regions (lista) y databases (lista). Si estas variables no son definidas entonces no seguirá el flujo de ejecución y pedirá que ingrese las variables mencionadas.
2. Se guardara en variables el nombre del snapshot mas antiguo para ambas regiones.
3. Se eliminara los snapshots más antiguos de las dos base de datos en las 2 regiones definidas.

### 4.3. Job template *<u>restore-db-cluster-snapshots</u>*

1. Recibe como variables de entrada region, database, engine y dbsubnetgroup. Si estas variables no son definidas entonces no seguirá el flujo de ejecución y pedirá que ingrese las variables mencionadas.

   Nota: la variable engine hace referencia a el motor de base de datos y dbsubnetgroup hace referencia al subnet group donde se creara la base de datos

2. Se guarda en una variable la ultima copia de snapshot.

3. Se crea una base de datos desde  el ultimo snapshot. Aquí usamos todas las variable definidas al principio con el fin de crear exitosamente el clúster de base de datos.

4. Se crea una instancia de base datos dentro del clúster que creamos anteriormente.

## 5. Construido con  🛠️

- **Ansible** para hacer uso de los módulos como shell ,pause , assert ,etc.
- **AWX** donde se ejecutará los job template y workflow. También donde se programara las ejecuciones.
- **AWS CLI** para obtener información y ejecutar acciones como crear, copiar y eliminar snapshots. Además, restaurar una base de datos desde un snapshot.
- **jq** para procesar JSON y obtener solo ciertos atributos de interés de la información obtenida de AWS.
- **Shell scripting (Linux)** para usar adecuadamente el modulo shell de Ansible.

## 6. Notas 📄

El siguiente diagrama muestra los tiempos de ejecución de crear y copiar snapshots.

<img src="D:\CANVIA\INFO\Replica de BD VIR-ORE\note.png" alt="note" style="zoom:75%;" />